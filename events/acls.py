import requests
import json


from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_weather_data(city, state):
    # get the lat and ln of the city
    params = {
        "q": f"{city},{state}, US",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    # content = json.loads(response.content)
    content = response.json()

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    # get the weather
    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_resp = response.json()
    try:
        temp = json_resp["main"]["temp"]
        desc = json_resp["weather"][0]["description"]
    except (KeyError, IndexError):
        return None

    return {
        "temp": temp,
        "desc": desc,
    }


def get_photo(city, state):
    headers = {"Authoization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + ' ' + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
