FROM python:3
# installs a existing image

# ENV PYTHONBUFFERED 1
# creates environmental variable that allows us to see python logs

WORKDIR /app
# creates a directory on our container to put files

COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py
# copies files from our local project folder to our container

RUN pip install -r requirements.txt
#run a terminal command in our container

CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
